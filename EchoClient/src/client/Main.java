package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		try {
			//Socket socket  = new Socket("localhost", 5000);
			Socket socket  = new Socket("10.146.1.34", 5000);
			BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);
			
			String echoString;
			String response;
			
			Scanner sc = new Scanner(System.in);
			do {
				System.out.println("Enter a string to be echo");
				echoString = sc.nextLine();
				stringToEcho.println(echoString);
				if(!echoString.equals("exit")) {
					response = echoes.readLine();
					System.out.println(response);
				}
			}while(!echoString.equals("exit"));
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}