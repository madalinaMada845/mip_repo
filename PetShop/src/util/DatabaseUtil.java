package util;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.scene.image.Image;
import model.Animal;
import model.Stapan;
import model.Programare;

public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop_Test");
		entityManager = entityManagerFactory.createEntityManager();

	}

	/**
	 * salveaza datele animalului in BD
	 * @param animal
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	/**
	 * salveaza datele proprietarului unui anumal in BD
	 * @param personalMedical
	 */
	public void saveStapan(Stapan stapan) {
		entityManager.persist(stapan);
	}
	
	
	
	/**
	 * salveaza datele programarii in BD
	 * @param programare
	 */
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}

	
	/**
	 * cautare animal in BD
	 * @param animal
	 */
	public void retrieveAnimal(Animal animal) {
		animal = entityManager.find(Animal.class, animal.getName());
	}
	
	
	
	/**
	 * cautare programare BD
	 * @param programare
	 */
	public void retrieveProgramare(Programare programare) {
		programare = entityManager.find(Programare.class, programare.getNumarProgramare());
	}
	
	/**
	 * cautare proprietar BD
	 * @param programare
	 */
	public void retrieveProprietar(Stapan stapan) {
		stapan = entityManager.find(Stapan.class, stapan.getIdstapan());
	}
	
	/**
	 * update animal BD
	 * @param animal
	 */
	public void updateAnimal(Animal animal) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		String newAge=(scanner.next());
		String newWeight=(scanner.next());
		String newProblem=(scanner.next());
		retrieveAnimal(animal);
		startTransaction();
		animal.setName(newName);
		//animal.setAge(newAge);
		//animal.setWeight(newWeight);
		//animal.setProblem(newProblem);
		commitTransaction();
	}
	
	/**
	 * update owner BD
	 * @param owner
	 */
	public void updateOwner(Stapan stapan) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		String newAdress = (scanner.next());
		String newPhone = (scanner.next());
		retrieveProprietar(stapan);
		startTransaction();
		stapan.setStapanNume(newName);
		stapan.setStapanAdresa(newAdress);
		// owner.setPhone(newPhone);
		commitTransaction();
	}
	
	
	
	/**
	 * update programare
	 * @param programare
	 * @throws ParseException 
	 */
	public void updateProgramare(Programare programare) throws ParseException {
		Scanner scanner = new Scanner(System.in);
		String dateFormat = "dd/MM/yyyy";
		String newOra=scanner.next();
	    //programare.setData(new SimpleDateFormat(dateFormat).parse(scanner.nextLine()));
		//Date newData = scanner.next();
		retrieveProgramare(programare);
		startTransaction();
		//programare.setData(newData);
		commitTransaction();
	}
	
	/**
	 * sterge un animal din BD
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		retrieveAnimal(animal);
		entityManager.getTransaction().begin();
		entityManager.remove(animal);
		entityManager.getTransaction().commit();
	}
	
	
	
	/**
	 * sterge o programare din BD
	 * @param programare
	 */
	public void deleteProgramare(Programare programare) {
		retrieveProgramare(programare);
		entityManager.getTransaction().begin();
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}
	
	/**
	 * sterge un proprietar din BD
	 * @param owner
	 */
	public void deleteOwner(Stapan stapan) {
		retrieveProprietar(stapan);
		entityManager.getTransaction().begin();
		entityManager.remove(stapan);
		entityManager.getTransaction().commit();
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
		
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * afiseaza in consola toate animalele din BD
	 */
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		for(Animal animal : results) {
			System.out.println("Animal has ID: " + animal.getIdAnimal() +  ", breed" + animal.getBreed() + ", animal name is: " + animal.getName() );
		}
	}
	
	
	/**
	 * afiseaza in consola toti proprietarii din BD
	 */
	public void printAllOwnersFromDB() {
		List<Stapan> results1=entityManager.createNativeQuery("Select * from petshop.stapan", Stapan.class).getResultList();
		for(Stapan owner : results1) {
			System.out.println("Owner ID is: " + owner.getIdstapan()
			+ ",  name: " + owner.getStapanNume()
			+ ", addres:" + owner.getStapanAdresa());
			}
	}
	
	
	
	/**
	 * afiseaza in consola toate programrile din BD
	 */
	public void printAllProgramsFromDB() {
		List<Programare> results3=entityManager.createNativeQuery("Select * from petshop.programare", Programare.class).getResultList();
		for(Programare programare : results3) {
			System.out.println("Numarul programarii este:" + programare.getNumarProgramare()
			+ "In data de: " + programare.getData()
			+ "Animalul ce va intra: " + programare.getAnimal() );
			
		}
	}
	
	
	public List<Animal> animalList() {
		List<Animal> animalList=(List<Animal>)entityManager.createQuery("Select * from Animal a",Animal.class).getResultList();
		return animalList;
	}
	
	public List<Programare> listProgramari() {
		return entityManager.createNativeQuery("Select * from petshop.Programare", Programare.class).getResultList();
	}
}
	//asta o sa il folosesti cand o sa adaugi poze in baza de date
	/*public Image listImages(Programare value) {
		Image img = null;
		img = new Image(new ByteArrayInputStream(value.getAnimal().getImage()));
		
		return img;
		}*/
	
	
	//la functia asta va mai trebui sa iti adaugi in baza de date la animal o coloana cu probleme
	//si o sa folosesti la istoric 
	/*public String listAllHistory(Programare prog) {
		String list = null;
			List<Animal> results = entityManager.createNativeQuery("Select * from petclinic.animal, petclinic.programare where petclinic.animal.animal_id = petclinic.programare.animal_id", Animal.class)
					.getResultList();
			for(Animal animal: results) {
				if(prog.getAnimal().getIdAnimal() == animal.getIdAnimal())
					list = animal.getProblem();
			}
			
			return list;
		}*/
	