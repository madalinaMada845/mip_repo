package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="numar_programare")
	private int numarProgramare;

	@Temporal(TemporalType.DATE)
	private Date data;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animal_id")
	private Animal animal;

	public Programare() {
	}

	public int getNumarProgramare() {
		return this.numarProgramare;
	}

	public void setNumarProgramare(int numarProgramare) {
		this.numarProgramare = numarProgramare;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

}