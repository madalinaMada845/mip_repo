package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stapan database table.
 * 
 */
@Entity
@NamedQuery(name="Stapan.findAll", query="SELECT s FROM Stapan s")
public class Stapan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idstapan;

	@Column(name="stapan_adresa")
	private String stapanAdresa;

	@Column(name="stapan_nume")
	private String stapanNume;

	//bi-directional many-to-one association to Animal
	@OneToMany(mappedBy="stapan")
	private List<Animal> animals;

	public Stapan() {
	}

	public int getIdstapan() {
		return this.idstapan;
	}

	public void setIdstapan(int idstapan) {
		this.idstapan = idstapan;
	}

	public String getStapanAdresa() {
		return this.stapanAdresa;
	}

	public void setStapanAdresa(String stapanAdresa) {
		this.stapanAdresa = stapanAdresa;
	}

	public String getStapanNume() {
		return this.stapanNume;
	}

	public void setStapanNume(String stapanNume) {
		this.stapanNume = stapanNume;
	}

	public List<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

	public Animal addAnimal(Animal animal) {
		getAnimals().add(animal);
		animal.setStapan(this);

		return animal;
	}

	public Animal removeAnimal(Animal animal) {
		getAnimals().remove(animal);
		animal.setStapan(null);

		return animal;
	}

}