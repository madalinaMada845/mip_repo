package controllers;

import java.net.URL;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {

	private DatabaseUtil db = new DatabaseUtil();
	@FXML
	private ListView<Programare> listView = new ListView<>();
	
	
	@FXML
	private TextField breed;
	
	@FXML	
	private TextField animal;
	@FXML
	private TextField stapan;
	
	


	private ObservableList<Programare> lista = FXCollections.observableArrayList();
	private ObservableList<Programare> lista2 = FXCollections.observableArrayList();

	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());

		}
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lista.addAll(db.listProgramari());
		listView.setItems(lista);
		
		listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Programare>() {

			@Override
			public void changed(ObservableValue<? extends Programare> observableValue, Programare oldValue, Programare newValue) {
				
				
				animal.setText(newValue.getAnimal().getName());
				stapan.setText(newValue.getAnimal().getStapan().getStapanNume());
				breed.setText(newValue.getAnimal().getBreed());
				
				
			}
		});
		
		
		listView.setCellFactory(new Callback<ListView<Programare>, ListCell<Programare>>() {
			@Override
			public ListCell<Programare> call(ListView<Programare> p) {
				ListCell<Programare> cell = new ListCell<Programare>() {
					@Override
					protected void updateItem(Programare t, boolean bln) {
						int prog = 1;
						super.updateItem(t, bln);
						if (t != null)
							setText("Programare " + t.getNumarProgramare());
					}
				};
				return cell;
			}
		});

	}

}
