package main;

import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Stapan;

import model.Programare;
import util.DatabaseUtil;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root=(BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene=new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} 
	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		
		
		/*//primul stapan
		Stapan owner1 = new Stapan();
		owner1.setIdstapan(1);
		owner1.setStapanNume("Vlad Carla");
		
		owner1.setStapanAdresa("Brasov....");
		//owner1.setPhone("0767864125");
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveStapan(owner1);
		dbUtil.commitTransaction();
		//dbUtil.printAllOwnersFromDB();
		dbUtil.closeEntityManager();
		
		// 2-lea stapan
		Stapan owner2 = new Stapan();
		owner2.setIdstapan(2);
		owner2.setStapanNume("Popescu George");

		owner2.setStapanAdresa("Brasov....");
		// owner1.setPhone("0767864125");
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveStapan(owner2);
		dbUtil.commitTransaction();
		// dbUtil.printAllOwnersFromDB();
		dbUtil.closeEntityManager();
		
		// 3-lea stapan
		Stapan owner3 = new Stapan();
		owner3.setIdstapan(3);
		owner3.setStapanNume("Grecu Catalin");

		owner3.setStapanAdresa("Brasov....");
		// owner1.setPhone("0767864125");
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveStapan(owner3);
		dbUtil.commitTransaction();
	    dbUtil.printAllOwnersFromDB();
		dbUtil.closeEntityManager();
	
		//primul animal
		Animal animal1=new Animal();
		animal1.setIdAnimal(1);
		animal1.setBreed("Caine, Rasa: Pechinez");
		animal1.setName("Plusica");
		animal1.setStapan(owner1);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal1);
		dbUtil.commitTransaction();
		//dbUtil.printAllOwnersFromDB();
		dbUtil.closeEntityManager();
		
		// animal 2
		Animal animal2 = new Animal();
		animal2.setIdAnimal(2);
		animal2.setBreed("Caine, Rasa: Pomeranian");
		animal2.setName("Rex");
		animal2.setStapan(owner2);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal2);
		dbUtil.commitTransaction();
		// dbUtil.printAllOwnersFromDB();
		dbUtil.closeEntityManager();
		
		//  animal 3
		Animal animal3 = new Animal();
		animal3.setIdAnimal(3);
		animal3.setBreed("Caine, Rasa: Fara Rasa(Maidanez)");
		animal3.setName("Jessy");
		animal3.setStapan(owner3);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal3);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		
		//prima programare
		Programare programare1=new Programare();
		programare1.setNumarProgramare(0001);
		//programare1.setData("");
		//programare1.setOra("9:00");
		programare1.setAnimal(animal1);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveProgramare(programare1);
		dbUtil.commitTransaction();
		//dbUtil.printAllProgramsFromDB();
		dbUtil.closeEntityManager();
		
		//a 2-a programare
		Programare programare2=new Programare();
		programare2.setNumarProgramare(0002);
		//programare2.setData("10-07-2018");
		//programare2.setOra("12:00");
		programare2.setAnimal(animal2);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveProgramare(programare2);
		dbUtil.commitTransaction();
		//dbUtil.printAllProgramsFromDB();
		dbUtil.closeEntityManager();
		
		//a 3-a programare
		Programare programare3=new Programare();
		programare3.setNumarProgramare(0003);
		//programare3.setData();
		//programare3.setOra("8:30");
		programare3.setAnimal(animal3);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveProgramare(programare3);
		dbUtil.commitTransaction();
		dbUtil.printAllProgramsFromDB();
		dbUtil.closeEntityManager();
*/		
		
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.commitTransaction();
		launch(args);
		dbUtil.closeEntityManager();
			}

	

}
