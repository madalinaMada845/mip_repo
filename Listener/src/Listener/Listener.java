package Listener;

public class Listener {

	public static void main(String[] args) {
		LongRunningTask longRunningTask = new LongRunningTask();
		longRunningTask.setOnCompleteListener(new OnCompleteListener() {

//			@Override
//			public void OnComplete() {
//				System.out.println("Yeah long running task was complete");
//				
//			}
			
			@Override
			public void OnComplete(String test) {
				System.out.println(test + "Yeah long running task was complete");
				
			}
		});
		System.out.println("Starting the long running task");

	}

}

interface OnCompleteListener {
	//public void OnComplete();
	public void OnComplete(String test);
}

class LongRunningTask implements Runnable {

	private OnCompleteListener onCompleteListener;
	
	public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(5*1000);
			//onCompleteListener.OnComplete();
			onCompleteListener.OnComplete("param");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
}